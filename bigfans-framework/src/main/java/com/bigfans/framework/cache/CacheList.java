package com.bigfans.framework.cache;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * @Title: 
 * @Description: 缓存列表数据注解,默认循环结果集,将每一条存储到缓存中,但不缓存整个结果集
 * @author lichong 
 * @date 2016年1月18日 上午10:51:48 
 * @version V1.0
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface CacheList{

	/**
	 * 缓存key生成器
	 * @return
	 */
	Class<? extends CacheKeyGenerator> keyGenerator() default CacheKeyGenerator.class;
	
	
	/**
	 * 设置过期时间秒数
	 * @return
	 */
	String expireIn() default "";

	/**
	 * 设置缓存key，如果设置了这个选项就不会通过@CacheKeyGenerator 来生成key值。
	 * @return
	 */
	String key() default "";
}
