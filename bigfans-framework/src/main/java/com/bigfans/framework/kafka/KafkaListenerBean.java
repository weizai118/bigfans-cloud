package com.bigfans.framework.kafka;

import java.lang.reflect.Method;

public class KafkaListenerBean {

    private String id;
    private String topic;
    private Object bean;
    private Method listenerMethod;
    private Integer threadCount;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public Object getBean() {
        return bean;
    }

    public void setBean(Object bean) {
        this.bean = bean;
    }

    public Method getListenerMethod() {
        return listenerMethod;
    }

    public void setListenerMethod(Method listenerMethod) {
        this.listenerMethod = listenerMethod;
    }

    public Integer getThreadCount() {
        return threadCount;
    }

    public void setThreadCount(Integer threadCount) {
        this.threadCount = threadCount;
    }
}
