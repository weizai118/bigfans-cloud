package com.bigfans.framework.es.request;

import org.elasticsearch.search.sort.SortOrder;

public class QuerySort {

	protected String field;
	protected SortOrder order;

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public SortOrder getOrder() {
		return order;
	}

	public void setOrder(SortOrder order) {
		this.order = order;
	}

}
