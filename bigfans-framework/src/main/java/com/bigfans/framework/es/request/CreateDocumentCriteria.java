package com.bigfans.framework.es.request;

import java.io.Serializable;

import com.bigfans.framework.es.IndexDocument;

/**
 * 
 * @author ellison
 *
 */
public class CreateDocumentCriteria implements Serializable{

	private static final long serialVersionUID = 3896237394273756591L;
	private String index;
	private String type;
	private String alias;
	private IndexDocument document;

	public String getIndex() {
		return index;
	}

	public void setIndex(String index) {
		this.index = index;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public IndexDocument getDocument() {
		return document;
	}

	public void setDocument(IndexDocument document) {
		this.document = document;
	}

}
