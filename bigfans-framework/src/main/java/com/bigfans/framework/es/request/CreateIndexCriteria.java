
package com.bigfans.framework.es.request;

import java.io.Serializable;

import com.bigfans.framework.es.Mapping;
import com.bigfans.framework.es.schema.IndexSettingsBuilder;

/**
 * 
 * @author lichong
 *
 */
public class CreateIndexCriteria implements Serializable{

	private static final long serialVersionUID = -9143800486248506990L;
	private String id;
	private Long version;
	private String indexName;
	private String source;
	private String alias;
	private Mapping schemaBuilder;
	private IndexSettingsBuilder settingsBuilder;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public String getIndexName() {
		return indexName;
	}

	public void setIndexName(String indexName) {
		this.indexName = indexName;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}
	
	public void setAlias(String alias) {
		this.alias = alias;
	}
	
	public String getAlias() {
		return alias;
	}

	public Mapping getSchemaBuilder() {
		return schemaBuilder;
	}
	
	public IndexSettingsBuilder getSettingsBuilder() {
		return settingsBuilder;
	}

	public void setSettingsBuilder(IndexSettingsBuilder settingsBuilder){
		this.settingsBuilder = settingsBuilder;
	}
	
}
