package com.bigfans.systemservice.service.impl;

import com.bigfans.systemservice.model.Role;
import com.bigfans.systemservice.service.RoleService;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

/**
 * @author lichong
 * @create 2018-04-19 下午9:20
 **/
public class RoleServiceImpl implements RoleService {

    @Override
    @Transactional(readOnly = true)
    public List<Role> listRolesByUser(String userId) throws Exception {
        // TODO mock
        Role operator = new Role();
        operator.setName("operator");
        return Arrays.asList(operator);
    }
}
