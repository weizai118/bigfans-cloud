package com.bigfans.systemservice.service.impl;

import com.bigfans.framework.dao.BaseServiceImpl;
import com.bigfans.framework.utils.MD5;
import com.bigfans.systemservice.dao.UserDAO;
import com.bigfans.systemservice.model.User;
import com.bigfans.systemservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 用户服务类
 * @author lichong
 *
 */
@Service(UserServiceImpl.BEAN_NAME)
public class UserServiceImpl extends BaseServiceImpl<User> implements UserService {
	
	public static final String BEAN_NAME = "userService";
	
	private UserDAO userDAO;

	@Autowired
	private UserService _this;
	
	@Autowired
	public UserServiceImpl(UserDAO userDAO) {
		super(userDAO);
		this.userDAO = userDAO;
	}

	@Transactional
	public User regist(User user) throws Exception{
		user.setPassword(MD5.toMD5(user.getPassword()));
		super.create(user);
		return user;
	}

	@Transactional(readOnly=true)
	@Override
	public User login(String account, String password) throws Exception {
		return login(account , password , true);
	}
	
	@Override
	public User login(String username, String password, boolean md5) throws Exception {
		User example = new User();
		example.setUsername(username);
		if(md5){
			example.setPassword(MD5.toMD5(password));
		}else{
			example.setPassword(password);
		}
		User loginUser = load(example);
		return loginUser;
	}
	
	@Transactional(readOnly=true)
	@Override
	public boolean emailExist(String email) throws Exception {
		User userEntity = new User();
		userEntity.setEmail(email);
		Long count = super.count(userEntity);
		return count > 0;
	}
	
	@Transactional(readOnly=true)
	@Override
	public boolean accountExist(String username) throws Exception {
		User userEntity = new User();
		userEntity.setUsername(username);
		Long count = count(userEntity);
		return count > 0;
	}
}
