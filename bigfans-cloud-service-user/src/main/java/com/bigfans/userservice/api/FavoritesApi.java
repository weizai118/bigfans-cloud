package com.bigfans.userservice.api;

import com.bigfans.framework.Applications;
import com.bigfans.framework.CurrentUser;
import com.bigfans.framework.annotations.NeedLogin;
import com.bigfans.framework.web.BaseController;
import com.bigfans.framework.web.RestResponse;
import com.bigfans.userservice.model.Favorites;
import com.bigfans.userservice.service.FavoritesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lichong
 * @create 2018-03-31 上午9:05
 **/
@RestController
public class FavoritesApi extends BaseController {

    @Autowired
    private FavoritesService favoritesService;

    @PostMapping(value="/favorites")
    @NeedLogin
    public RestResponse add(@RequestParam(name="prodId") String prodId) throws Exception{
        CurrentUser currentUser = Applications.getCurrentUser();
        Favorites entity = new Favorites();
        entity.setUserId(currentUser.getUid());
        entity.setProductId(prodId);
        favoritesService.create(entity);
        return RestResponse.ok();
    }

}
