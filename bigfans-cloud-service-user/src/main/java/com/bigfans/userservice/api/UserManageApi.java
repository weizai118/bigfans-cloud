package com.bigfans.userservice.api;

import com.bigfans.framework.web.RestResponse;
import com.bigfans.userservice.model.User;
import com.bigfans.userservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lichong
 * @create 2018-04-13 下午10:10
 **/
@RestController
public class UserManageApi {

    @Autowired
    private UserService userService;

    @GetMapping(value = "/users/{userId}")
    public RestResponse getUser(@PathVariable(value = "userId") String userId) throws Exception {
        User user = userService.load(userId);
        return RestResponse.ok(user);
    }

}
