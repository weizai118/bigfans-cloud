package com.bigfans.userservice.service;

import com.bigfans.framework.dao.BaseService;
import com.bigfans.userservice.model.Coupon;

import java.util.List;

/**
 * 
 * @Description:
 * @author lichong
 * 2015年7月10日上午9:14:39
 *
 */
public interface CouponService extends BaseService<Coupon> {
	
	boolean checkAcquirable(String userId, String couponId, Integer amount) throws Exception;
}
