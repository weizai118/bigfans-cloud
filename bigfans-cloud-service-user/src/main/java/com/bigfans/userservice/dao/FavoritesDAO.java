package com.bigfans.userservice.dao;

import com.bigfans.framework.dao.BaseDAO;
import com.bigfans.userservice.model.Favorites;

import java.util.List;

/**
 * 
 * @Description: 
 * @author lichong
 * @date Mar 10, 2016 3:26:27 PM 
 *
 */
public interface FavoritesDAO extends BaseDAO<Favorites> {

	List<Favorites> listByUser(String userId, Long start, Long pagesize);
	
	Favorites getByUser(String userId, String prodId);
	
}
