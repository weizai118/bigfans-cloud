package com.bigfans.orderservice.service.impl;

import com.bigfans.framework.dao.BaseServiceImpl;
import com.bigfans.orderservice.dao.PayMethodDAO;
import com.bigfans.orderservice.model.PayMethod;
import com.bigfans.orderservice.service.PayMethodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 
 * @Description:支付类型服务类
 * @author lichong
 * 2015年4月4日下午9:29:26
 *
 */
@Service(PayMethodServiceImpl.BEAN_NAME)
public class PayMethodServiceImpl extends BaseServiceImpl<PayMethod> implements PayMethodService {

	public static final String BEAN_NAME = "payMethodService";
	
	private PayMethodDAO payTypeDAO;
	
	@Autowired
	public PayMethodServiceImpl(PayMethodDAO payTypeDAO) {
		super(payTypeDAO);
		this.payTypeDAO = payTypeDAO;
	}

	@Override
	public List<PayMethod> listOnlinePayMethods() throws Exception {
		return payTypeDAO.listByType(PayMethod.TYPE_ONLINE);
	}
	
	@Override
	public PayMethod getByCode(String code) throws Exception {
		return payTypeDAO.getByCode(code);
	}
	
	@Override
	public List<PayMethod> listTopMethods() throws Exception {
		return payTypeDAO.listTopMethods();
	}
	
}
