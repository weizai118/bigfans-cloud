package com.bigfans.searchservice.service;

import com.bigfans.framework.model.FTSPageBean;
import com.bigfans.searchservice.model.Brand;

public interface BrandService {

    Brand getById(String brandId);

    FTSPageBean<Brand> search(String keyword, String categoryId, int start, int pagesize) throws Exception;

    void createIndexAndMapping() throws Exception;

    void createIndex() throws Exception;

    void createMapping() throws Exception;
}
