package com.bigfans.searchservice.service.impl;

import com.bigfans.framework.dao.BaseServiceImpl;
import com.bigfans.searchservice.dao.AttributeValueDAO;
import com.bigfans.searchservice.model.AttributeValue;
import com.bigfans.searchservice.service.AttributeValueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 
 * @Title:
 * @Description:属性值服务类
 * @author lichong
 * @date 2015年10月7日 上午11:52:42
 * @version V1.0
 */
@Service(AttributeValueServiceImpl.BEAN_NAME)
public class AttributeValueServiceImpl extends BaseServiceImpl<AttributeValue> implements
		AttributeValueService {

	public static final String BEAN_NAME = "attributeValueService";
	
	private AttributeValueDAO attrValueDAO;
	
	@Autowired
	public AttributeValueServiceImpl(AttributeValueDAO attrValueDAO) {
		super(attrValueDAO);
		this.attrValueDAO = attrValueDAO;
	}

	@Override
	public List<AttributeValue> listByOptionId(String attrId) throws Exception {
		return attrValueDAO.listByAttribute(attrId);
	}

	@Override
	public List<AttributeValue> listByProduct(String pid) throws Exception {
		return attrValueDAO.listByProduct(pid);
	}

	@Override
	public List<AttributeValue> listById(List<String> idList) throws Exception {
		return attrValueDAO.listById(idList);
	}
}
