package com.bigfans.model.event.tag;

import lombok.Data;

/**
 * @author lichong
 * @create 2018-03-18 下午5:29
 **/
@Data
public class TagCreatedEvent {

    private String id;
    private String name;
    private Integer relatedCount;

    public TagCreatedEvent() {
    }

    public TagCreatedEvent(String id) {
        this.id = id;
    }
}
