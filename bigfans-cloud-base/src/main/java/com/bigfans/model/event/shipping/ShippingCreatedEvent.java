package com.bigfans.model.event.shipping;

import com.bigfans.framework.event.AbstractEvent;

public class ShippingCreatedEvent extends AbstractEvent {

    private String id;

    public ShippingCreatedEvent(String id) {
        this.id = id;
    }
}
