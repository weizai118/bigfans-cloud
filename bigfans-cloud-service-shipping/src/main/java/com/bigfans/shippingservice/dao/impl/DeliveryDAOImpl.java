package com.bigfans.shippingservice.dao.impl;

import com.bigfans.framework.dao.MybatisDAOImpl;
import com.bigfans.shippingservice.dao.DeliveryDAO;
import com.bigfans.shippingservice.model.Delivery;
import org.springframework.stereotype.Repository;


/**
 * 
 * @Description:配送DAO操作
 * @author lichong
 * 2015年1月17日下午7:36:35
 *
 */
@Repository(DeliveryDAOImpl.BEAN_NAME)
public class DeliveryDAOImpl extends MybatisDAOImpl<Delivery> implements DeliveryDAO {

	public static final String BEAN_NAME = "deliveryDAO";
}
