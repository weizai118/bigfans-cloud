package com.bigfans.catalogservice.dao.impl;

import com.bigfans.catalogservice.dao.FloorDAO;
import com.bigfans.catalogservice.model.Floor;
import com.bigfans.framework.dao.MybatisDAOImpl;
import com.bigfans.framework.dao.ParameterMap;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 
 * @Description:
 * @author lichong
 * 2015年5月14日上午11:05:06
 *
 */
@Repository(FloorDAOImpl.BEAN_NAME)
public class FloorDAOImpl extends MybatisDAOImpl<Floor> implements FloorDAO {

	public static final String BEAN_NAME = "floorDAO";
	
	@Override
	public List<Floor> listFloor() {
		ParameterMap params = new ParameterMap();
		params.put("pageable" , false);
		return getSqlSession().selectList(className + ".list" , params);
	}

	@Override
	public int insertFloor(Floor example) {
		return 0;
	}
}
