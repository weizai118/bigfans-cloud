package com.bigfans.catalogservice.model;

import com.bigfans.catalogservice.model.entity.SpecValueEntity;

/**
 * 
 * @Title:
 * @Description: 商品规格值
 * @author lichong
 * @date 2015年9月21日 上午11:25:41
 * @version V1.0
 */
public class SpecValue extends SpecValueEntity {

	private static final long serialVersionUID = -7982565066340145527L;

	@Override
	public String toString() {
		return "SpecValue [value=" + value + ", optionId=" + optionId + ", imagePath=" + imagePath + ", categoryId="
				+ categoryId + ", id=" + id + ", createDate=" + createDate + ", updateDate=" + updateDate
				+ ", deleted=" + deleted + "]";
	}

}
