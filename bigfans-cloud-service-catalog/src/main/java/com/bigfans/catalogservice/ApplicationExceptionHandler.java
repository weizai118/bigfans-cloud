package com.bigfans.catalogservice;

import com.bigfans.catalogservice.exception.OutOfStockException;
import com.bigfans.framework.exception.GlobalExceptionHandler;
import com.bigfans.framework.web.RestResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ApplicationExceptionHandler extends GlobalExceptionHandler {

    @ExceptionHandler(value = OutOfStockException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    public RestResponse orderCreateExceptionHandler(OutOfStockException ex){
        return RestResponse.error(ex.getProdId() + ":out of stock");
    }

}
